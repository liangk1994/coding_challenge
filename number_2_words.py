'''
Usage: python3 number_2_words.py
'''
Dictionary = {
	0:"zero",
	1:"one",
	2:"two",
	3:"three",
	4:"four",
	5:"five",
	6:"six",
	7:"seven",
	8:"eight",
	9:"nine",
	10:"ten",
	11:"eleven",
	12:"twelve",
	13:"thirteen",
	14:"fourteen",
	15:"fifteen",
	16:"sixteen",
	17:"seventeen",
	18:"eighteen",
	19:"nineteen",
	20:"twenty",
	30:"thirty",
	40:"forty",
	50:"fifty",
	60:"sixty",
	70:"seventy",
	80:"eighty",
	90:"ninety",
	100:"hundred",
	1000:"thousand",
	1000000:"million"
	}


raw_input = str(input("please enter a number: "))
raw_input = raw_input.split(".")
while(len(raw_input[0]) > 9):
	raw_input = input("Billions are not supported. Please enter another number: ")
	raw_input = raw_input.split(".")
splited_input = []
for i in range(len(raw_input[0])):
		splited_input.append(int(raw_input[0][i]))

last_group = (len(splited_input)-1)//3
the_string = ""
for i in range(len(splited_input)):
	current_index = (len(splited_input)-1-i)%3+1
	current_group = (len(splited_input)-1-i)//3
	if (splited_input[i] == 0):
		continue
	if(last_group == current_group):
		if(current_index == 1):
			temp = splited_input[i]
			the_string += Dictionary[splited_input[i]] + " "
		elif(current_index == 2):
			the_string += Dictionary[splited_input[i]*10] + " "
		elif(current_index == 3):
			the_string+=Dictionary[splited_input[i]] + " " + Dictionary[100] + " and "
	else:
		if(last_group == 2):
			the_string+=Dictionary[1000000]+ " "
		
		elif(last_group == 1):
			the_string+=Dictionary[1000] + " "
		the_string+=Dictionary[splited_input[i]] + " " + Dictionary[100] + " and "
	last_group = current_group


if (len(raw_input) != 1):
	the_string += "point "
	for i in range(len(raw_input[1])):
		the_string+= Dictionary[int(raw_input[1][i])] + " "
print(the_string)
		

