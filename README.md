# number to words & words to number

This are two small programs that turn number to words or words to number

## Installation

Make sure python3(version 3.6.7) is installed correctly on your machine


## Usage

```bash
python3 number_2_words.py
```

ex. input 2345 will output "two thousand three hundred and forty five "

```bash
python3 words_2_number.py
```
ex. input "two thousand three hundred and forty five" will output 2345


## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

